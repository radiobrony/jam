import React from 'react';
import {Modal} from './Modal';
import {PrimaryButton} from './Button';

export default function InteractionModal({submit, close}) {
  return (
    <Modal close={close}>
      <h1>Autoriser la sortie son</h1>
      <br />
      <p>Cliquez sur le bouton OK pour autoriser cette page à jouer du son.</p>
      <br />
      <p>
        <PrimaryButton
          onClick={() => {
            submit();
            close();
          }}
        >
          OK
        </PrimaryButton>
      </p>
    </Modal>
  );
}
