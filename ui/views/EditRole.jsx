import React from 'react';
import {addRole, removeRole, leaveStage} from '../logic/room';
import {addAdmin, removeAdmin, useIdentityAdminStatus} from '../logic/admin';
import {currentId} from '../logic/identity';
import {state} from '../logic/main';
import {use} from 'use-minimal-state';
import {openModal} from './Modal';
import EditIdentity from './EditIdentity';
import {useMqParser} from '../logic/tailwind-mqp';
import {ButtonContainer, SecondaryButton} from './Button';
import StreamingModal from './StreamingModal';

export default function EditRole({peerId, speakers, moderators, onCancel}) {
  let mqp = useMqParser();
  let [myAdminStatus] = useIdentityAdminStatus(currentId());
  let [peerAdminStatus] = useIdentityAdminStatus(peerId);
  return (
    <div className={mqp('md:p-10')}>
      {myAdminStatus?.admin && (
        <div>
          <h3 className="font-medium">Actions d'administration</h3>
          <br />
          {(peerAdminStatus?.admin && (
            <button
              onClick={() => removeAdmin(peerId).then(onCancel)}
              className={
                'mb-2 h-12 px-6 text-lg text-black bg-gray-200 rounded-lg focus:shadow-outline active:bg-gray-300 mr-2'
              }
            >
              ❎️ Retirer admin
            </button>
          )) || (
            <button
              onClick={() => addAdmin(peerId).then(onCancel)}
              className={
                'mb-2 h-12 px-6 text-lg text-black bg-gray-200 rounded-lg focus:shadow-outline active:bg-gray-300 mr-2'
              }
            >
              👑️ Ajouter admin
            </button>
          )}
          <br />
          <br />
          <hr />
          <br />
        </div>
      )}
      <h3 className="font-medium">Actions de modération</h3>
      <br />
      <button
        onClick={() => addRole(peerId, 'speakers').then(onCancel)}
        className={
          speakers.includes(peerId)
            ? 'hidden'
            : 'mb-2 h-12 px-6 text-lg text-black bg-gray-200 rounded-lg focus:shadow-outline active:bg-gray-300 mr-2'
        }
      >
        ↑ Inviter sur la scène
      </button>
      <button
        onClick={() => removeRole(peerId, 'speakers').then(onCancel)}
        className={
          speakers.includes(peerId)
            ? 'mb-2 h-12 px-6 text-lg text-black bg-gray-200 rounded-lg focus:shadow-outline active:bg-gray-300 mr-2'
            : 'hidden'
        }
      >
        ↓ Déplacer vers l'audience
      </button>
      <button
        onClick={() => addRole(peerId, 'moderators').then(onCancel)}
        className={
          !speakers.includes(peerId) || moderators.includes(peerId)
            ? 'hidden'
            : 'mb-2 h-12 px-6 text-lg text-black bg-gray-200 rounded-lg focus:shadow-outline active:bg-gray-300 mr-2'
        }
      >
        ✳️ Passer en modérateur
      </button>
      <button
        onClick={() => removeRole(peerId, 'moderators').then(onCancel)}
        className={
          moderators.includes(peerId)
            ? 'mb-2 h-12 px-6 text-lg text-black bg-gray-200 rounded-lg focus:shadow-outline active:bg-gray-300 mr-2'
            : 'hidden'
        }
      >
        ❎ Retirer de la modération
      </button>
      <button
        onClick={onCancel}
        className="mb-2 h-12 px-6 text-lg text-black bg-gray-100 rounded-lg focus:shadow-outline active:bg-gray-300"
      >
        Annuler
      </button>
      <br />
      <br />
      <hr />
    </div>
  );
}

export function EditSelf({onCancel}) {
  let mqp = useMqParser();
  let myPeerId = currentId();
  let [iSpeak, iModerate, room] = use(state, [
    'iAmSpeaker',
    'iAmModerator',
    'room',
  ]);
  return (
    <div className={mqp('md:p-10')}>
      <h3 className="font-medium">Actions</h3>
      <br />
      <ButtonContainer>
        {!room.access?.lockedIdentities && (
          <SecondaryButton
            onClick={() => {
              openModal(EditIdentity);
              onCancel();
            }}
          >
            Editer profil
          </SecondaryButton>
        )}
        {iModerate && !iSpeak && (
          <SecondaryButton
            onClick={() => addRole(myPeerId, 'speakers').then(onCancel)}
          >
            ↑ Aller sur scène
          </SecondaryButton>
        )}
        {iModerate && iSpeak && (
          <SecondaryButton
            onClick={() => removeRole(myPeerId, 'speakers').then(onCancel)}
          >
            ↓ Quitter la scène
          </SecondaryButton>
        )}
        {!iModerate && iSpeak && (
          <SecondaryButton
            onClick={() => {
              leaveStage();
              onCancel();
            }}
          >
            ↓ Quitter la scène
          </SecondaryButton>
        )}
        {iModerate && iSpeak && (
          <SecondaryButton
            onClick={() => {
              openModal(StreamingModal);
              onCancel();
            }}
          >
            Diffuser de l'audio
          </SecondaryButton>
        )}
        <SecondaryButton light onClick={onCancel}>
          Annuler
        </SecondaryButton>
      </ButtonContainer>
      <br />
      <br />
      <hr />
    </div>
  );
}
