import React, {useState, useMemo} from 'react';
import slugify from 'slugify';

import {createRoom, updateApiQuery} from '../logic/backend';
import {currentId} from '../logic/identity';
import {navigate} from '../lib/use-location';
import {enterRoom, state} from '../logic/main';
import Container from './Container';

export default function Start({urlRoomId, roomFromURIError}) {
  let [name, setName] = useState('');
  let [description, setDescription] = useState('');
  let [color, setColor] = useState('#4B5563');
  let [logoURI, setLogoURI] = useState('');
  let [buttonText, setButtonText] = useState('');
  let [buttonURI, setButtonURI] = useState('');

  const [showAdvanced, setShowAdvanced] = useState(false);

  let submit = e => {
    e.preventDefault();
    state.set('userInteracted', true);
    let roomId;
    if (name) {
      let slug = slugify(name, {lower: true, strict: true});
      roomId = slug + '-' + Math.random().toString(36).substr(2, 4);
    } else {
      roomId = Math.random().toString(36).substr(2, 6);
    }

    (async () => {
      let roomCreated = await createRoom(
        roomId,
        name,
        description,
        logoURI,
        color,
        currentId()
      );
      if (roomCreated) {
        updateApiQuery(`/rooms/${roomId}`, roomCreated, 200);
        if (urlRoomId !== roomId) navigate('/' + roomId);
        enterRoom(roomId);
      }
    })();
  };

  let humins = useMemo(() => {
    let humins = ['DoubleMalt', 'mitschabaude', '__tosh'];
    return humins.sort(() => Math.random() - 0.5);
  }, []);

  return (
    <Container style={{height: 'initial', minHeight: '100%'}}>
      <div className="p-6 md:p-10">
        <div
          className={
            roomFromURIError
              ? 'mb-12 p-4 text-gray-700 rounded-lg border border-yellow-100 bg-yellow-50'
              : 'hidden'
          }
        >
          L'ID du salon{' '}
          <code className="text-gray-900 bg-yellow-200">
            {window.location.pathname.substring(1)}
          </code>{' '}
          n'est pas valide.
          <br />
          <br />
          Vous pouvez créer un nouveau salon ci-dessous.
        </div>

        <h1>Créer un salon</h1>

        <p className="text-gray-600">
          Cliquez sur le bouton ci-dessous pour commencer.
        </p>

        <form className="pt-6" onSubmit={submit}>
          <div className="hidden">
            <input
              className="rounded placeholder-gray-400 bg-gray-50 w-full md:w-96"
              type="text"
              placeholder="Titre"
              value={name}
              name="jam-room-topic"
              autoComplete="off"
              onChange={e => {
                setName(e.target.value);
              }}
            ></input>
            <div className="p-2 text-gray-500 italic">
              Sujet du salon.{' '}
              <span className="text-gray-400">(optionel)</span>
            </div>
            <br />
            <textarea
              className="rounded placeholder-gray-400 bg-gray-50 w-full md:w-full"
              placeholder="Description du salon"
              value={description}
              name="jam-room-description"
              autoComplete="off"
              rows="2"
              onChange={e => {
                setDescription(e.target.value);
              }}
            ></textarea>
            <div className="p-2 text-gray-500 italic">
              Décrivez le but du salon.{' '}
              <span className="text-gray-400">
                (optionel) (supporte le{' '}
                <a
                  className="underline"
                  href="https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf"
                  target="_blank"
                  rel="noreferrer"
                >
                  Markdown
                </a>
                )
              </span>{' '}
              <span onClick={() => setShowAdvanced(!showAdvanced)}>
                {/* heroicons/gift */}
                <svg
                  style={{cursor: 'pointer'}}
                  className="pb-1 h-5 w-5 inline-block"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M12 8v13m0-13V6a2 2 0 112 2h-2zm0 0V5.5A2.5 2.5 0 109.5 8H12zm-7 4h14M5 12a2 2 0 110-4h14a2 2 0 110 4M5 12v7a2 2 0 002 2h10a2 2 0 002-2v-7"
                  />
                </svg>
              </span>
            </div>
          </div>

          {/* advanced Room options */}
          <div className={showAdvanced ? '' : 'hidden'}>
            <br />
            <input
              className="rounded placeholder-gray-400 bg-gray-50 w-full md:w-full"
              type="text"
              placeholder="Logo URL"
              value={logoURI}
              name="jam-room-logo-uri"
              autoComplete="off"
              onChange={e => {
                setLogoURI(e.target.value);
              }}
            ></input>
            <div className="p-2 text-gray-500 italic">
              URL vers un logo carré.{' '}
              <span className="text-gray-400">(optionel)</span>
            </div>

            <br />
            <input
              className="rounded w-44 h-12"
              type="color"
              value={color}
              name="jam-room-color"
              autoComplete="off"
              onChange={e => {
                setColor(e.target.value);
              }}
            ></input>
            <div className="p-2 text-gray-500 italic">
              Une couleur pour votre salon.{' '}
              <span className="text-gray-400">(optionel)</span>
            </div>

            <br />
            <input
              className="rounded placeholder-gray-400 bg-gray-50 w-full md:w-full"
              type="text"
              placeholder="URL du bouton"
              value={buttonURI}
              name="jam-room-button-uri"
              autoComplete="off"
              onChange={e => {
                setButtonURI(e.target.value);
              }}
            ></input>
            <div className="p-2 text-gray-500 italic">
              Définir le lien pour le {`'bouton principal'`}.{' '}
              <span className="text-gray-400">(optionel)</span>
            </div>

            <br />
            <input
              className="rounded placeholder-gray-400 bg-gray-50 w-full md:w-96"
              type="text"
              placeholder="Texte du bouton"
              value={buttonText}
              name="jam-room-button-text"
              autoComplete="off"
              onChange={e => {
                setButtonText(e.target.value);
              }}
            ></input>
            <div className="p-2 text-gray-500 italic">
              Définir le texte pour le {`'bouton principal'`}.{' '}
              <span className="text-gray-400">(optionel)</span>
            </div>
          </div>

          <button
            onClick={submit}
            className="select-none h-12 px-6 text-lg text-black bg-gray-200 rounded-lg focus:shadow-outline active:bg-gray-300"
          >
            🌱 Créer le salon
          </button>
        </form>

        <hr className="mt-14 mb-14" />

        <h1>Bienvenue sur Jam</h1>

        <div className="flex flex-row pt-4 pb-4">
          <div className="flex-1 text-gray-600 pt-6">
            Instance Jam de Radio Brony.
          </div>
          <div className="flex-initial">
            <img
              className="mt-8 md:mt-4 md:mb-4 md:mr-8"
              style={{width: 130, height: 130}}
              alt="Jam mascot by @eejitlikeme"
              title="Jam mascot by @eejitlikeme"
              src="/img/jam.jpg"
            />
          </div>
        </div>

        <div className="pt-32 text-xs text-gray-400 text-center">
          <a
            href="https://gitlab.com/jam-systems/jam"
            target="_blank"
            rel="noreferrer"
          >
            créé
          </a>{' '}
          avec ♥ par{' '}
          {humins.map((humin, idx) => (
            <span key={idx}>
              {' '}
              <a
                href={'https://twitter.com/' + humin}
                target="_blank"
                rel="noreferrer"
              >
                @{humin}
              </a>
            </span>
          ))}{' '}
          à Berlin &amp; Vienne,{' '}
          <a
            href="https://www.digitalocean.com"
            target="_blank"
            rel="noreferrer"
          >
            hébergé à Frankfurt
          </a>
        </div>
      </div>
    </Container>
  );
}
